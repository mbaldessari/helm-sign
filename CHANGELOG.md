# Changelog

## v1.0.0

  * Added official support for Python 3.11

## v0.1.3

  * Do not require helm charts to be named with semver versions (!1 by @candrews)
  * Updated python-gnupg to 0.5.*

## v0.1.2

  * Added official support for Python 3.9 and 3.10
  * Updated semver to 2.13
  * Updated PyYAML to 6.0

## v0.1.1

  * Increased version number for getting packages uploaded to PyPi

## v0.1.0

  * First Release
